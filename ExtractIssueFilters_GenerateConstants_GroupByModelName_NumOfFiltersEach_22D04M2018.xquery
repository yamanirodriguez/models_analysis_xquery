(:declare namespace ns1="http://www.onenetwork.com/Platform"; :)
xquery version "3.0";

declare namespace functx = "http://www.functx.com";
declare function functx:camel-case-to-words 
( $arg as xs:string? , $delim as xs:string )  as xs:string {
    concat(substring($arg,1,1),
    replace(substring($arg,2),'(\p{Lu})',
    concat($delim, '$1')))
};

(: string-join((codepoints-to-string(10),"FilterName","Type","CountOfIssuesUsingIt"),","), :)
concat(codepoints-to-string(10),"ModelName",",","FilterName",",","FilterType",",","CountOfIssuesUsingIt",codepoints-to-string(10)),
let $items := (
     doc("file:/Z:/views/RTVN-21.0/apps/OMS/Module%20Process%20Template.mpt.files/SCC.Claim.modelDef")/ModuleModelDef/ModuleLevelDef/Computation/CompFilter,
     doc("file:/Z:/views/RTVN-21.0/apps/OMS/Module%20Process%20Template.mpt.files/OMS.Contract.modelDef")/ModuleModelDef/ModuleLevelDef/Computation/CompFilter,
     doc("file:/Z:/views/RTVN-21.0/apps/OMS/Module%20Process%20Template.mpt.files/InvoiceHold.modelDef")/ModuleModelDef/ModuleLevelDef/Computation/CompFilter,
     doc("file:/Z:/views/RTVN-21.0/apps/OMS/Module%20Process%20Template.mpt.files/Invoice.modelDef")/ModuleModelDef/ModuleLevelDef/Computation/CompFilter,
     doc("file:/Z:/views/RTVN-21.0/apps/OMS/Module%20Process%20Template.mpt.files/OrderForecast.modelDef")/ModuleModelDef/ModuleLevelDef/Computation/CompFilter,
     doc("file:/Z:/views/RTVN-21.0/apps/OMS/Module%20Process%20Template.mpt.files/SCC.OrderMilestone.modelDef")/ModuleModelDef/ModuleLevelDef/Computation/CompFilter,
     doc("file:/Z:/views/RTVN-21.0/apps/OMS/Module%20Process%20Template.mpt.files/OMS.RfqBid.modelDef")/ModuleModelDef/ModuleLevelDef/Computation/CompFilter,
     doc("file:/Z:/views/RTVN-21.0/apps/OMS/Module%20Process%20Template.mpt.files/SCC.OrderEvent.modelDef")/ModuleModelDef/ModuleLevelDef/Computation/CompFilter,
     (: Misssed receipt <model> 10/05/2018 :)
     doc("file:/z:/views/RTVN-21.0/apps/SCProcessTemplate/Module Process Template.mpt.files/SCC.Receipt.modelDef")/ModuleModelDef/ModuleLevelDef/Computation/CompFilter
          
)
return
for $Filter in $items
    let $IssueModelNameOrig:=$Filter/../ComputationRef/CustomModelName
    let $IssueModelName := 
        if (contains($IssueModelNameOrig,"OMS")) then (
            substring($IssueModelNameOrig,5)
        ) else if (contains($IssueModelNameOrig,"SCC")) then (
            substring($IssueModelNameOrig,5)
        ) else (
            $IssueModelNameOrig
        )
    let $IssueName:=$Filter/../ComputationRef/Name
    let $FilterType:=$Filter/Type
    (: order by concat(escape-html-uri(string-join(($IssueName,$Filter/Name/text())/normalize-space(),",")),codepoints-to-string(10)) :)
    (: group by $FilterNameGroup:=string-join(($IssueModelName,$Filter/Name,$Filter/Type),",") :)
    group by $FilterNameGroup:=string-join(($IssueModelName,$Filter/Name,$Filter/Type),",")
    (: order by $FilterNameGroup :)
    order by count($IssueName) descending,$FilterNameGroup
    return concat($FilterNameGroup,",",count($IssueName),",",string-join((upper-case(functx:camel-case-to-words(($Filter/Name)[1],"_"))),""),codepoints-to-string(10))
    (: return concat(escape-html-uri(string-join(($Filter/Name/text(),$IssueName)/normalize-space(),",")),codepoints-to-string(10)) :)
