(:declare namespace ns1="http://www.onenetwork.com/Platform";  
xquery version "3.0";  :)  


(: let $SpecificFilter := 'ProductGroupLevel' :)
let $SpecificFilter := 'ItemCategory' (: Target filter; the goal is to place PGL filter after it in all issues :) 

let $ProductGroupLevel_Node := "
                <CompFilter>
                    <Name>ProductGroupLevel</Name>
                    <Type>ModelRefs</Type>
                    <Required>false</Required>
                    <AllowMultiple>true</AllowMultiple>
                    <LevelType>ProductGroupLevel</LevelType>
                    <ModelLinkHandling>
                        <CustomModelName>ProductGroupLevel</CustomModelName>
                        <PickerReportDefName>OMS.ProductGroupLevels</PickerReportDefName>
                    </ModelLinkHandling>
                </CompFilter>"
                
(: let $EmergencyOderOnly_Node := '<CompFilter><Name>EmergencyOrderOnly</Name><Type>Boolean</Type><Required>false</Required></CompFilter>' :)
let $Issues := (
    (: Test file 
    doc("file:/C:/YamaniRodriguez_XDeveloper/_07_July_2018/PDS-9334--Mismatch in item and alert filed contents/FiltersAnalysis/AlertFilterUpdates/SCC.EnhancedOrder.cp.modelDef")/ModuleModelDef/ModuleLevelDef/Computation  :)
    (: doc("file:/Y:/SCC.EnhancedOrder.updated.modelDef")/ModuleModelDef/ModuleLevelDef/Computation :) 
    doc("file:/Z:/views/RTVN-21.0/apps/OMS/Module%20Process%20Template.mpt.files/SCC.EnhancedOrder.modelDef")/ModuleModelDef/ModuleLevelDef/Computation
)

for $Computation at $pos in $Issues
    let $IssueName:=$Computation/ComputationRef/Name
    let $filters := (
        $Computation/CompFilter/Name[text() = $SpecificFilter]
    )   
    (: order by count($filters) descending,$IssueName :)
    return
        if(count($filters) > 0) then (
            
            (: comment to see results :)
            copy $x := $Computation
            modify (
                (: insert nodes $EmergencyOderOnly_Node as last into $x :)
                (: insert nodes saxon:parse($EmergencyOderOnly_Node) as last into $x :)
                (: replace node $Computation/CompFilter/Name[text() = $SpecificFilter] with saxon:parse($ProductGroupLevel_Node) :)
                (: insert nodes saxon:parse($ProductGroupLevel_Node) as last into $x :)
                (: insert nodes saxon:parse($ProductGroupLevel_Node) into $x/CompFilter/Name[text() = $SpecificFilter] :)
                (: replace node $x/CompFilter/Name[text() = $SpecificFilter] with saxon:parse($ProductGroupLevel_Node) :)
                (: insert nodes saxon:parse($ProductGroupLevel_Node) after $x/CompFilter/Name[text() = $SpecificFilter] :)
                (: insert nodes $ProductGroupLevel_Node after $x/CompFilter/Name[text() = $SpecificFilter]/.. :)
                insert nodes saxon:parse($ProductGroupLevel_Node) after $x/CompFilter/Name[text() = $SpecificFilter]/..
            )
            return $x
        ) else (
            $Computation
        )
        
        